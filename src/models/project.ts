/* **************************  Enum Type  *************************** 
	Used for and with the below Project Type class because the Project Type Class
	property, status, has exactly two options. In the context of the Project Type 
	object's status property, we don't need text that makes sense to humans that we
	store as a value, but we just need some identifier. 
******************************************************************************* */

export enum ProjectStatus {
	Active,
	Finished,
}

/* **************************  Project Type class  *************************** 
	A dedicated class that we can use to give us the ability of creating a project
	that always has the same structure. I decided to create a class and not a custom
	type or interface, because I want to be able to instantiate it. 
******************************************************************************* */

export class Project {
	constructor(
		public id: string,
		public title: string,
		public description: string,
		public people: number,
		public status: ProjectStatus
	) {}
}
