import { ProjectInput } from "./components/project-input.js";
import { ProjectList } from "./components/project-list.js";

/* Instantiate an new ProjectInput so that we render
 the form inside of the template */
new ProjectInput();
new ProjectList("active");
new ProjectList("finished");
