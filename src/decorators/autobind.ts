/* Binder Decorator
This is a method decorator for the purpose of adding it
to the submitHandler method to bind "this" */

export function binder(
	/* Use "_" for names so TS knows we don't plan on using
	them and then won't throw an error */
	_: any,
	_2: string,
	descriptor: PropertyDescriptor
) {
	/* Get access to the original method */
	const originalMethod = descriptor.value;
	const adjustedDescriptor: PropertyDescriptor = {
		configurable: true,
		get() {
			const boundFunction = originalMethod.bind(this);
			return boundFunction;
		},
	};
	return adjustedDescriptor;
}
