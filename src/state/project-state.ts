import { Project, ProjectStatus } from "../models/project.js";
/* **************************  Custom Type *************************** */
type Listener<T> = (items: T[]) => void;

/* **************************  State Base Class  *************************** 
	A class which has the listeners array and an add listeners method.
 ******************************************************************************* */

class State<T> {
	protected listeners: Listener<T>[] = [];

	addListener(listenerFn: Listener<T>) {
		this.listeners.push(listenerFn);
	}
}

/* **************************  Project State Management Class  *************************** 
	A class that manages the state of our app, which manages our projects
	or whichever state we might need to manage in the end. 
	Also, this class would then allow us to setup listeners
	in the different part of the apps which are interested. 
	This is a familiar pattern used in such frameworks like React with REDUX,
    where you have a global state management object where you just listen to changes. 
 ******************************************************************************* */

export class ProjectState extends State<Project> {
	/* Private class property that stores projects */

	private projects: Project[] = [];

	private static instance: ProjectState;

	/* Private constructor to guarantee that
	 this is a singleton class */

	private constructor() {
		super();
	}

	static getInstance() {
		if (this.instance) {
			return this.instance;
		}
		this.instance = new ProjectState();
		return this.instance;
	}

	/* Public method that creates a new project that can 
	be stored in our private projects property */

	addProject(title: string, description: string, numberOfPeople: number) {
		const newProject = new Project(
			Math.random().toString(),
			title,
			description,
			numberOfPeople,
			ProjectStatus.Active
		);

		this.projects.push(newProject);
		for (const listenerFn of this.listeners) {
			listenerFn(this.projects.slice());
		}
	}

	moveProject(projectId: string, newStatus: ProjectStatus) {
		const project = this.projects.find((prj) => prj.id === projectId);
		if (project && project.status !== newStatus) {
			project.status = newStatus;
			this.updateListeners();
		}
	}

	private updateListeners() {
		for (const listenerFn of this.listeners) {
			listenerFn(this.projects.slice());
		}
	}
}

/* Global instance of ProjectState class that
 we can use from this entire file. */

export const projectState = ProjectState.getInstance();
