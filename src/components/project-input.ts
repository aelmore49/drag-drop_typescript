import { Component } from "./base-component.js";
import { ValidationContainer, validate } from "../utils/project-validation.js";
import { binder } from "../decorators/autobind.js";
import { projectState } from "../state/project-state.js";
/* **************************  Project Input class  *************************** */

/* In charge of gathering and validating form input from the user */
export class ProjectInput extends Component<HTMLDivElement, HTMLFormElement> {
	titleInputElement: HTMLInputElement;
	descriptionInputElement: HTMLInputElement;
	peopleInputElement: HTMLInputElement;
	constructor() {
		super("project-input", "app", true, "user-input");

		/* Get all the form inputs and submit button */

		this.titleInputElement = this.element.querySelector(
			"#title"
		)! as HTMLInputElement;

		this.descriptionInputElement = this.element.querySelector(
			"#description"
		)! as HTMLInputElement;

		this.peopleInputElement = this.element.querySelector(
			"#people"
		)! as HTMLInputElement;

		/* Call configure method which acts as 
		our event listener for the form */
		this.configure();
	}

	/*  method that gets called 
	from the constructor and sets our
	event listener for the form. */

	configure() {
		this.element.addEventListener("submit", this.submitHandler);
	}

	renderContent() {}

	/* Private method to gather all the inputs from the user 
	   Return value as tuple type. */

	private gatherUserInput(): [string, string, number] | void {
		const title = this.titleInputElement.value;
		const description = this.descriptionInputElement.value;
		const people = this.peopleInputElement.value;

		const titleValidatable: ValidationContainer = {
			value: title,
			required: true,
		};
		const descriptionValidatable: ValidationContainer = {
			value: description,
			required: true,
			minLength: 5,
		};
		const peopleValidatable: ValidationContainer = {
			value: +people,
			required: true,
			min: 1,
			max: 5,
		};

		/* validate user input */

		if (
			!validate(titleValidatable) ||
			!validate(descriptionValidatable) ||
			!validate(peopleValidatable)
		) {
			alert("Invalid input!");
			return;
		} else {
			return [title, description, +people];
		}
	}

	/* Private method to clear user input
	 after successfully retrieving it. */

	private clearInputs() {
		this.titleInputElement.value = "";
		this.descriptionInputElement.value = "";
		this.peopleInputElement.value = "";
	}

	@binder
	/* Private method that gets called from
	 configure() on form submit */
	private submitHandler(event: Event) {
		event.preventDefault();
		const userInput = this.gatherUserInput();
		if (Array.isArray(userInput)) {
			const [title, description, people] = userInput;
			projectState.addProject(title, description, people);
			this.clearInputs();
		}
	}
}
